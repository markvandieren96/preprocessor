use std::collections::HashMap;

#[derive(Default, Debug)]
pub struct ListDefine {
	lists: HashMap<String, Vec<String>>,
}

impl ListDefine {
	pub fn try_parse(s: &str) -> Result<ListDefine, String> {
		let mut line_iter = s.lines();
		let mut lists = HashMap::new();
		let headers = {
			if let Some(first_line) = line_iter.next() {
				let first_line = first_line.trim();
				let tokens: Vec<&str> = first_line.split_whitespace().collect();
				for token in &tokens {
					lists.insert((*token).to_owned(), Vec::new());
				}
				tokens
			} else {
				return Err("Could not parse header value of list define".to_owned());
			}
		};

		for line in line_iter {
			let line = line.trim();
			if line.is_empty() {
				continue;
			}
			let tokens: Vec<&str> = line.split_whitespace().collect();
			if tokens.len() != headers.len() {
				return Err("List entry did not have the same number of elements as header".to_owned());
			}

			for (header, token) in headers.iter().zip(tokens.iter()) {
				lists.get_mut(header.to_owned()).unwrap().push((*token).to_owned());
			}
		}
		Ok(ListDefine { lists })
	}

	pub fn get(&self, tag: &str) -> Option<&Vec<String>> {
		self.lists.get(tag)
	}
}
