#[derive(Clone)]
pub struct Config {
	pub comment_start: String,
	pub comment_end: String,
}

impl Config {
	pub fn macro_start(&self) -> String {
		format!("{}<", self.comment_start)
	}

	pub fn macro_end(&self) -> String {
		format!(">{}", self.comment_end)
	}

	pub fn gen_start(&self) -> String {
		format!("{}<gen>{}", self.comment_start, self.comment_end)
	}

	pub fn gen_end(&self) -> String {
		format!("{}</gen>{}", self.comment_start, self.comment_end)
	}
}
