#[derive(Debug)]
pub enum Section {
	Data(String),
	MacroInput(String),
	MacroOutput(String),
}

pub fn contains_macro_input(sections: &[Section]) -> bool {
	for section in sections {
		if let Section::MacroInput(_) = section {
			return true;
		}
	}
	false
}

pub fn export(sections: &[Section]) -> String {
	let mut buffer = String::new();

	for section in sections {
		match section {
			Section::Data(s) => buffer += s,
			Section::MacroInput(s) => buffer += s,
			Section::MacroOutput(s) => buffer += s,
		}
	}

	buffer
}
