use super::*;
use std::path::{Path, PathBuf};

pub struct Engine {
	config: Config,
	files: Vec<File>,
	defines: Vec<ListDefine>,
	patterns: Vec<PatternDefine>,
}

impl Engine {
	pub fn new(config: Config) -> Engine {
		Engine {
			config,
			files: Vec::new(),
			defines: Vec::new(),
			patterns: Vec::new(),
		}
	}

	pub fn ingest(&mut self, file: &Path) -> Result<(), io::Error> {
		let text = io::read_text(file)?;

		let mut reader = Reader::new(self.config.clone());
		for line in text.lines() {
			reader.read(line);
		}
		let sections = reader.finish();

		if !contains_macro_input(&sections) {
			return Ok(());
		}

		let file = File { sections, path: file.to_owned() };
		self.files.push(file);

		Ok(())
	}

	pub fn run(&mut self) {
		println!("Engine collected {} file(s)", self.files.len());

		println!("Scanning files for macros...");
		// Scan pass
		for file in &self.files {
			for section in &file.sections {
				if let Section::MacroInput(text) = section {
					let text = trim_macro(&self.config, text);
					if text.is_empty() {
						continue;
					}

					if text.starts_with(DEFINE_LIST_TAG) {
						let text = trim_start(text, DEFINE_LIST_TAG).trim();
						match ListDefine::try_parse(text) {
							Ok(list) => {
								self.defines.push(list);
							}
							Err(e) => println!("Failed to parse list definition: {}", e),
						}
					} else if text.starts_with(DEFINE_PATTERN_TAG) {
						let text = trim_start(text, DEFINE_PATTERN_TAG).trim();
						match PatternDefine::try_parse(text) {
							Ok(pattern) => {
								self.patterns.push(pattern);
							}
							Err(e) => println!("Failed to parse pattern definition: {}", e),
						}
					}
				}
			}
		}

		println!("Running macros...");
		// Replace pass
		for file in &mut self.files {
			expand_sections(&self.config, &self.defines, &self.patterns, &mut file.sections);
			let s = export(&file.sections);

			match io::read_text(&file.path) {
				Ok(original) => {
					if original != s {
						println!("Writing to \"{}\"", file.path.display());
						if let Err(e) = io::write_text(&file.path, &s) {
							println!("Error writing file \"{}\": {}", file.path.display(), e);
						}
					} else {
						println!("No changes: \"{}\"", file.path.display());
					}
				}
				Err(e) => {
					println!("Failed to read file \"{}\": {}", file.path.display(), e);
				}
			}
		}
		println!("Finished");
	}
}

pub struct File {
	pub sections: Vec<Section>,
	pub path: PathBuf,
}
