use super::*;

pub fn expand_sections(config: &Config, list_defines: &[ListDefine], patterns: &[PatternDefine], sections: &mut Vec<Section>) {
	let mut index = 0;
	while let Some(section) = sections.get(index) {
		if let Some(text) = expand_section(config, section, list_defines, patterns) {
			let text = format!("{}\n{}{}\n", config.gen_start(), text, config.gen_end());
			let section = Section::MacroOutput(text);
			if sections.len() == index + 1 {
				sections.push(section);
			} else {
				sections.insert(index + 1, section);
			}
		}
		index += 1;
	}
}

fn expand_section(config: &Config, section: &Section, list_defines: &[ListDefine], patterns: &[PatternDefine]) -> Option<String> {
	if let Section::MacroInput(text) = section {
		let text = trim_macro(config, text);
		if text.is_empty() {
			return None;
		}

		if text.starts_with(EXPAND_TAG) {
			let text = trim_start(text, EXPAND_TAG).trim();

			match expand_string(text, list_defines) {
				Ok(s) => {
					return Some(expand_string_patterns(&s, patterns).unwrap());
				}
				Err(e) => println!("Failed to parse expand: {}", e),
			}
		}
	}
	None
}

fn expand_string(s: &str, list_defines: &[ListDefine]) -> Result<String, String> {
	let re = regex::Regex::new(r"\{(\w*)\}").expect("Failed to compile regex");

	let mut out = Vec::new();
	for line in s.lines() {
		let mut index = 0;
		let mut group_size = 0;
		loop {
			let result = re.replace_all(line, |caps: &regex::Captures| {
				if let Some(cap) = caps.get(1) {
					let tag = cap.as_str();
					if let Some(list) = resolve(tag, list_defines) {
						if group_size == 0 {
							group_size = list.len();
						}
						if group_size != list.len() {
							"<Substitution group did not have the same number of members>".to_owned()
						} else if let Some(item) = list.get(index) {
							item.clone()
						} else {
							panic!("Expansion failed")
						}
					} else if tag.is_empty() {
						// this block catches the {} format specifier
						"{}".to_owned()
					} else {
						format!("<Failed to resolve tag: {{{}}}>", tag)
					}
				} else {
					"<Macro replace failed>".to_owned()
				}
			});

			out.push(result + "\n");
			index += 1;
			if index >= group_size {
				break;
			}
		}
	}

	let mut s = String::new();
	for sub in out {
		s += &sub;
	}
	Ok(s)
}

fn resolve<'a>(tag: &str, list_defines: &'a [ListDefine]) -> Option<&'a Vec<String>> {
	for list in list_defines.iter() {
		let res = list.get(tag);
		if res.is_some() {
			return res;
		}
	}
	None
}
