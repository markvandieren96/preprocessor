use super::*;

enum ReadingState {
	Data,
	Macro,
	MacroOutput,
}

impl Default for ReadingState {
	fn default() -> ReadingState {
		ReadingState::Data
	}
}

pub struct Reader {
	config: Config,
	state: ReadingState,
	sections: Vec<Section>,
	read_buffer: String,
}

impl Reader {
	pub fn new(config: Config) -> Reader {
		Reader {
			config,
			state: ReadingState::default(),
			sections: Vec::new(),
			read_buffer: String::new(),
		}
	}

	pub fn read(&mut self, line: &str) {
		let trimmed_line = line.trim();
		match self.state {
			ReadingState::Data => {
				if trimmed_line.starts_with(&self.config.gen_start()) {
					self.finish_section(ReadingState::MacroOutput);
				} else if trimmed_line.starts_with(&self.config.macro_start()) {
					self.finish_section(ReadingState::Macro);
					if trimmed_line.ends_with(&self.config.macro_end()) {
						self.push_line(line);
						self.finish_section(ReadingState::Data);
					} else {
						self.push_line(line);
					}
				} else {
					self.push_line(line);
				}
			}
			ReadingState::Macro => {
				if trimmed_line.ends_with(&self.config.macro_end()) {
					self.push_line(line);
					self.finish_section(ReadingState::Data);
				} else {
					self.push_line(line);
				}
			}
			ReadingState::MacroOutput => {
				if trimmed_line.ends_with(&self.config.gen_end()) {
					self.finish_section(ReadingState::Data);
				}
			}
		}
	}

	pub fn finish(mut self) -> Vec<Section> {
		self.finish_section(ReadingState::Data);
		self.sections
	}

	fn push_line(&mut self, line: &str) {
		self.read_buffer += line;
		self.read_buffer += "\n";
	}

	fn finish_section(&mut self, new_state: ReadingState) {
		if !self.read_buffer.is_empty() {
			match self.state {
				ReadingState::Data => self.sections.push(Section::Data(self.read_buffer.clone())),
				ReadingState::Macro => self.sections.push(Section::MacroInput(self.read_buffer.clone())),
				ReadingState::MacroOutput => (),
			};
		}

		self.read_buffer = String::new();
		self.state = new_state;
	}
}
