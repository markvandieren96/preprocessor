use super::*;

pub fn trim_macro<'a>(config: &Config, text: &'a str) -> &'a str {
	trim_end_tag(config, trim_start_tag(config, text.trim())).trim()
}

pub fn trim_start<'a>(s: &'a str, pat: &str) -> &'a str {
	if s.starts_with(pat) {
		&s[pat.len()..]
	} else {
		s
	}
}

pub fn trim_end<'a>(s: &'a str, pat: &str) -> &'a str {
	if s.ends_with(pat) {
		&s[..s.len() - pat.len()]
	} else {
		s
	}
}

pub fn trim_start_tag<'a>(config: &Config, s: &'a str) -> &'a str {
	trim_start(s, &config.macro_start())
}

pub fn trim_end_tag<'a>(config: &Config, s: &'a str) -> &'a str {
	trim_end(s, &config.macro_end())
}
