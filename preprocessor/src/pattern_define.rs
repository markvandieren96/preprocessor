#[derive(Default, Debug)]
pub struct PatternDefine {
	name: String,
	pattern: String,
}

impl PatternDefine {
	pub fn try_parse(s: &str) -> Result<PatternDefine, String> {
		let mut line_iter = s.lines();
		let pattern_name = {
			if let Some(first_line) = line_iter.next() {
				first_line.trim()
			} else {
				return Err("Could not parse define pattern name".to_owned());
			}
		};

		let lines: Vec<&str> = line_iter.collect();
		let pattern = lines.join("\n");

		Ok(PatternDefine { name: pattern_name.to_owned(), pattern })
	}

	pub fn expand(&self, inputs: &[(String, String)]) -> Option<String> {
		let mut s = self.pattern.clone();
		for (key, val) in inputs {
			s = s.replace(&format!("{{{}}}", key), val);
		}

		// Remove any unmatched tags
		let re = regex::Regex::new(r"\{\w*\}").expect("Failed to compile regex");
		Some(re.replace_all(&s, |_caps: &regex::Captures| "".to_owned()).to_string())
	}
}

pub fn expand_string_patterns(s: &str, patterns: &[PatternDefine]) -> Result<String, String> {
	let re = regex::Regex::new(r"\{(?P<name>\w*?): (?P<args>.*)\}").expect("Failed to compile regex");
	let re_args = regex::Regex::new(r#"(?P<key>\w*)="(?P<val>[^"]*)""#).expect("Failed to compile regex");

	let mut out = Vec::new();
	for line in s.lines() {
		let result = re.replace_all(line, |caps: &regex::Captures| {
			let name = &caps["name"];
			let args = &caps["args"];

			let caps: Vec<(String, String)> = re_args.captures_iter(args).map(|cap| (cap["key"].to_owned(), cap["val"].to_owned())).collect();
			patterns.iter().find(|pattern| pattern.name == name).and_then(|pattern| pattern.expand(&caps)).unwrap_or_else(|| "<Macro replace failed>".to_owned())
		});

		out.push(result + "\n");
	}

	let mut s = String::new();
	for sub in out {
		s += &sub;
	}
	Ok(s)
}
