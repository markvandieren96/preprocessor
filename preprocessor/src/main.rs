use clap::{App, Arg};
use std::error::Error;

mod config;
pub use config::*;

mod engine;
use engine::*;

mod expand;
use expand::*;

mod list_define;
use list_define::*;

mod pattern_define;
use pattern_define::*;

mod reader;
use reader::*;

mod section;
use section::*;

mod tag;
use tag::*;

mod trim;
use trim::*;

fn main() -> Result<(), Box<dyn Error>> {
	#[rustfmt::skip]
	let matches =
		App::new("preprocessor")
			.version("1.0")
			.author("Mark <markvandieren96@gmail.com>")
			.about("")
			.arg(Arg::from_usage("-d, --dir=[FOLDER] 'The working directory'"))
			.arg(Arg::from_usage("-b, --blacklist=[String;String] 'A list of ; separated blacklisted directories'"))
			.arg(Arg::from_usage("-e, --extensions=[String;String] 'A list of ; separated file extensions'"))
			.arg(Arg::from_usage("-c, --comment=[/* */] 'The comment format to use, seperated by a space'"))
		.get_matches();

	let input = matches.value_of("dir").unwrap_or_default();
	let blacklist: Vec<&str> = matches.value_of("blacklist").unwrap_or_default().split(';').collect();
	let extensions: Vec<&str> = matches.value_of("extensions").unwrap_or("rs").split(';').collect();
	let comment_format: Vec<&str> = matches.value_of("comment").unwrap_or("/* */").split(' ').collect();
	let (comment_start, comment_end) = if let [start, end] = comment_format.as_slice() {
		(*start, *end)
	} else {
		panic!("Invalid comment format!");
	};

	println!("Dir: {}", input);
	println!("Blacklist: {:?}", blacklist);
	println!("Extensions: {:?}", extensions);
	println!("Comment format: {} {}", comment_start, comment_end);
	let files = io::find_all_files_with_blacklist(input, &blacklist);
	println!("Found {} files", files.len());

	let config = Config {
		comment_start: comment_start.to_owned(),
		comment_end: comment_end.to_owned(),
	};

	let mut engine = Engine::new(config);

	for file in files.iter() {
		if let Some(file_extension) = file.extension().and_then(|file_extension| file_extension.to_str()) {
			for ext in extensions.iter() {
				if *ext == file_extension {
					if let Err(e) = engine.ingest(file) {
						println!("Error reading file \"{}\": {}", file.display(), e);
					}
					break;
				}
			}
		}
	}
	engine.run();
	Ok(())
}
